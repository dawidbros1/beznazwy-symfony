<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ArticleRepository")
 */
class Article
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $author;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $DOI;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $ministerialPoints;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $impactFactor;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $placeOfPublication;

    /**
     * @ORM\Column(type="date")
     */
    private $dateOfPublication;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAuthor(): ?string
    {
        return $this->author;
    }

    public function setAuthor(string $author): self
    {
        $this->author = $author;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDOI(): ?string
    {
        return $this->DOI;
    }

    public function setDOI(string $DOI): self
    {
        $this->DOI = $DOI;

        return $this;
    }

    public function getMinisterialPoints(): ?string
    {
        return $this->ministerialPoints;
    }

    public function setMinisterialPoints(string $ministerialPoints): self
    {
        $this->ministerialPoints = $ministerialPoints;

        return $this;
    }

    public function getImpactFactor(): ?string
    {
        return $this->impactFactor;
    }

    public function setImpactFactor(string $impactFactor): self
    {
        $this->impactFactor = $impactFactor;

        return $this;
    }

    public function getPlaceOfPublication(): ?string
    {
        return $this->placeOfPublication;
    }

    public function setPlaceOfPublication(string $placeOfPublication): self
    {
        $this->placeOfPublication = $placeOfPublication;

        return $this;
    }

    public function getDateOfPublication(): ?\DateTimeInterface
    {
        return $this->dateOfPublication;
    }

    public function setDateOfPublication(\DateTimeInterface $dateOfPublication): self
    {
        $this->dateOfPublication = $dateOfPublication;

        return $this;
    }
}
