<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class EditArticleController extends AbstractController
{
    public function index()
    {
        return $this->render('editArticle/editArticle.html.twig', [
            'controller_name' => 'EditArticleController',
        ]);
    }
}
