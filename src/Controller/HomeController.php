<?php

namespace App\Controller;


use App\Entity\Useer;
use App\Entity\Article;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Dompdf\Dompdf;
use Dompdf\Options;

class HomeController extends AbstractController
{
    public function index(Request $request)
    {

        if ($this->get('session')->get('maxArticle') !== null) {
            $maxArticle = $this->get('session')->get('maxArticle');



            // if ($form2->isSubmitted() && $form2->isValid()) {
            //     var_dump("cesc");
            //     die();
            // }
        }
		
		




        $form = $this->createFormBuilder(null)
            ->add('Wyszukiwarka', TextType::class, array('attr' => array(
                'placeholder' => 'Wyszukaj',
                'class' => 'searcher'
            )))
            ->add('Title', CheckboxType::class, array(
                'attr' => array(
                    'checked'   => 'checked',
                    'disabled' => true,
                ),
            ))
            ->add('Author', CheckboxType::class, ['required' => false])
            ->add('Doi', CheckboxType::class, ['required' => false])
            ->add('Mp', CheckboxType::class, ['required' => false])
            ->add('If', CheckboxType::class, ['required' => false])
            ->add('Dop', CheckboxType::class, ['required' => false])
            ->add('Pop', CheckboxType::class, ['required' => false])
            ->add('save', SubmitType::class, array(
                'label' => 'Wyszukaj',
            ))->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $userRepository = $this->getDoctrine()->getRepository(Useer::class);
            $users = $userRepository->findAll();

            $articleRepository = $this->getDoctrine()->getRepository(Article::class);
            $articles = $articleRepository->findAll();

            $date = $request->request->get('form');
            $query = $date['Wyszukiwarka'];

            $ids = $this->simpleSearch($query, $users, $articles, $articleRepository, $userRepository);

            $myArticles = [];

            foreach ($ids as $id) {
                $myArticles = array_merge($myArticles, [$articleRepository->find($id)]);
            }

            $checkBoxValue = ['Title', 'Author', 'Doi', 'Mp', 'If', 'Dop', 'Pop'];

            $checkBoxScore = [];

            foreach ($checkBoxValue as $key => $value) {
                if (isset($date[$value])) {
                    $checkBoxScore[$key] = 1;
                } else {
                    $checkBoxScore[$key] = 0;
                }
            }


            // $this->get('session')->set('maxArticle', count($myArticles));

            $maxArticle = count($myArticles);

            $form2 = $this->createFormBuilder(null)
                ->add('save', SubmitType::class);

            for ($i = 0; $i < $maxArticle; $i++) {
                $form2 = $form2->add('id' . $i, CheckboxType::class);
            }


            $form2 = $form2->getForm();

            $form2->handleRequest($request);


            return $this->render('home/score.html.twig', array(
                'myArticles' => $myArticles,
                'checkBoxScore' => $checkBoxScore,
                'form2' => $form2->createView(),
            ));
        }

        return $this->render('home/index.html.twig', array(
            'form' => $form->createView()
        ));
    }

    function simpleSearch($text, $users, $articles, $articleRepository, $userRepository)
    {
        $scoreID = []; //Miejsce na końcowe ID
        $usersID = [];
        $arrayOfKeyWords = textToArray($text);


        //Sprawdzenie czy wyszukiwanie jest po dacie - odpowiednia reguła
        if (preg_match('/^[0-9]{4}-[0-9]{4}$/', $text)) {

            $dataMin = "";
            $dataMax = "";

            for ($i = 0; $i < 4; $i++) {
                $dataMin .= $text[$i];
                $dataMax .= $text[$i + 5];
            }

            $dataMin = intval($dataMin);
            $dataMax = intval($dataMax);

            //Wyszukiwanie po dacie od DATA1-DATA2 (Dokładnie taki format)
            foreach ($articles as $article) {

                $dataYear = $article->getDateOfPublication()->format('Y');
                $article_id = $article->getId();

                if ($dataYear >= $dataMin && $dataYear <= $dataMax) {
                    $scoreID[count($scoreID)] = $article_id;
                }
            }

            return $scoreID;
        }

        //Koniec wyszukiwanie po dacie 

        //Wyszukiwanie po autorach
        foreach ($users as $user) {
            foreach ($arrayOfKeyWords as $keyWord) {
                $keyWord = strtoupper($keyWord);
                if (strtoupper($user->getFirstName()) == $keyWord || strtoupper($user->getLastName())  == $keyWord) {
                    $usersID[count($usersID)] = $user->getId();
                }
            }
        }
        //Koniec przeszukania autorach

        // Przeszukanie artykułów
        function search($item, $keyWord, &$scoreID, $article_id)
        {
            $arrayOfKeyItem = textToArray($item);

            foreach ($arrayOfKeyItem as $keyItem) {
                $keyItem = strtoupper($keyItem);

                if ($keyItem == $keyWord) {
                    $scoreID[count($scoreID)] = $article_id;
                }
            }
        }

        foreach ($articles as $article) {
            $author = $article->getAuthor();
            $title = $article->getTitle();
            $DOI = $article->getDOI();
            $DOP = $article->getDateOfPublication()->format('Y-m-d');
            $POP = $article->getPlaceOfPublication();
            $article_id = $article->getId();


            // var_dump($DOP->format('Y-m-d'));
            // die();

            foreach ($arrayOfKeyWords as $keyWord) {
                $keyWord = strtoupper($keyWord);
                search($title, $keyWord, $scoreID, $article_id);
                search($DOI, $keyWord, $scoreID, $article_id);
                search($DOP, $keyWord, $scoreID, $article_id);
                search($POP, $keyWord, $scoreID, $article_id);
                search($author, $keyWord, $scoreID, $article_id);
            }
        }

        //Koniec przeszukiwania artykułów

        $userNames = [];

        foreach ($usersID as $userID) {
            $username = $userRepository->find($userID)->getFirstName() . ' ' . $userRepository->find($userID)->getLastName();
            $userNames =  array_merge($userNames, [$username]);
        }

        $score = $articleRepository->findByArticleIdByUserNames($userNames);
        $scoreID = array_merge($scoreID, $score);

        return array_unique($scoreID);
    }

    public function report(Request $request)
    {
        $i = 0;
        $ids = [];
        while (isset($request->get('form')[$i])) {
            $ids[$i] = ($request->get('form')[$i]);
            $i = $i + 1;
        }

        $pdfOptions = new Options();
        $pdfOptions->set('defaultFont', 'Arial');
        $dompdf = new Dompdf($pdfOptions);

        $checkBoxScore = [];
        $checkBoxScore[1] = $request->get('form')['author'];
        $checkBoxScore[2] = $request->get('form')['doi'];
        $checkBoxScore[3] = $request->get('form')['mp'];
        $checkBoxScore[4] = $request->get('form')['if'];
        $checkBoxScore[5] = $request->get('form')['dop'];
        $checkBoxScore[6] = $request->get('form')['pop'];

        $articleRepository = $this->getDoctrine()->getRepository(Article::class);
        $myArticles = [];

        foreach ($ids as $id) {
            $myArticles = array_merge($myArticles, [$articleRepository->find($id)]);
        }

        $html = $this->renderView('home/pdf.html.twig', [
            'checkBoxScore' => $checkBoxScore,
            'myArticles' => $myArticles,

        ]);

        $dompdf->loadHtml($html);
        $dompdf->setPaper('A4', 'landscape');
        $dompdf->render();

        $dompdf->stream("mypdf.pdf", [
            "Attachment" => true
        ]);
    }
}

function textToArray($text)
{
    $indexOfArray = 0;
    $arrayOfWords = [""];


    // var_dump($text);
    $lengthOfText = strlen($text);

    for ($i = 0; $i < $lengthOfText; $i++) {

        if ($text[$i] == " ") {
            $indexOfArray++;
            $arrayOfWords[$indexOfArray] = "";
        } else {
            $arrayOfWords[$indexOfArray] .= $text[$i];
        }
    }
    return $arrayOfWords;
}
