<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class RegulationsController extends AbstractController
{
    public function index()
    {
        return $this->render('regulations/regulations.html.twig', [
            'controller_name' => 'RegulationsController',
        ]);
    }
}
