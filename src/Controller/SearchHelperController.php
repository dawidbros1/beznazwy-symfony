<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class SearchHelperController extends AbstractController
{
    public function index()
    {
        return $this->render('searchHelper/searchHelper.html.twig', [
            'controller_name' => 'SearchHelperController',
        ]);
    }
}
