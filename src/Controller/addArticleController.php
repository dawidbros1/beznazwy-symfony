<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class AddArticleController extends AbstractController
{
    public function index()
    {
        return $this->render('addArticle/addArticle.html.twig', [
            'controller_name' => 'AddArticleController',
        ]);
    }
}
