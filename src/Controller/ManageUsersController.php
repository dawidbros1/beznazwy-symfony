<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class ManageUsersController extends AbstractController
{
    public function index()
    {
        return $this->render('manageUsers/manageUsers.html.twig', [
            'controller_name' => 'ManageUsers',
        ]);
    }
}