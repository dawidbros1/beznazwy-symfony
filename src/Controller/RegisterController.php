<?php

namespace App\Controller;

use App\Entity\Useer;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class RegisterController extends AbstractController
{
    public function register(Request $request)
    {
        $user = new Useer();

        $form = $this->createFormBuilder($user)
            ->add('firstName', TextType::class, array('attr' => array('placeholder' => 'Imie')))
            ->add('lastName', TextType::class, array('attr' => array('placeholder' => 'Nazwisko')))
            ->add('email', TextType::class, array('attr' => array('placeholder' => 'Email')))
            ->add('password', TextType::class, array('attr' => array('placeholder' => 'Hasło')))
            ->add('affiliation', TextType::class, array('attr' => array('placeholder' => 'Afiliacja')))
            ->add('save', SubmitType::class, array(
                'label' => 'Zarejestruj się',
            ))->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user = $form->getData();

            $entityManager = $this->getDoctrine()->getManager();

            $entityManager->persist($user);
            $entityManager->flush();

            return $this->redirect($this->generateUrl('home'));
        }

        return $this->render('register/index.html.twig', array(
            'form' => $form->createView()
        ));
    }
}
