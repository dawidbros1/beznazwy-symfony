<?php

namespace App\Controller;

use App\Entity\Users;
use App\Repository\UsersRepository;
use PDO;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Session\Session;

class LoginController extends AbstractController
{
    public function index(Request $request)
    {
        $session = new Session();
        $user = new Users();

        $form = $this->createFormBuilder($user)
            ->add('email', TextType::class, array('attr' => array('placeholder' => 'Email')))
            ->add('password', TextType::class, array('attr' => array('placeholder' => 'Hasło')))
            ->add('save', SubmitType::class, array(
                'label' => 'Zaloguj się',
            ))->getForm();


        $form->handleRequest($request);

        //Podano dane logowania
        if ($form->isSubmitted() && $form->isValid()) {
            // $entityManager = $this->getDoctrine()->getManager();
            $email = $form["email"]->getData();
            $password = $form["password"]->getData();
            $repository = $this->getDoctrine()->getRepository(Users::class);

            $myUser = $repository->findOneBy(array('email' => $email, 'password' => $password));


            if ($myUser) {
                //Poprawne logowanie
                $session->set('userId', $myUser->getId());
                $session->set('userRole', $myUser->getRole());

                // $this->twig->addGlobal('session', $session);

                return $this->render('login/welcomeUsers.html.twig', [
                    'controller_name' => 'RegisterController',
                ]);
            } else {
                //Nie poprawne logowania

                return $this->render('login/failLogin.html.twig', [
                    'controller_name' => 'RegisterController',
                ]);
            }
        }

        return $this->render('login/index.html.twig', array(
            'form' => $form->createView()
        ));
    }
}
