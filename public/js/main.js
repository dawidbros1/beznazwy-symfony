function newAuthor() {
    var authors = document.getElementById("authors");
    var arrayValue = [];
    var arrayShares = [];
    var myAuthors = document.getElementsByClassName('authors');
    var myShares = document.getElementsByClassName('shares');
    var maxAuthors = myAuthors.length;

    for (var i = 0; i < maxAuthors; i++) {
        arrayValue[i] = myAuthors[i].value;
        arrayShares[i] = myShares[i].value;
    }

    authors.innerHTML = '<label class="labelAuthors">Autorzy: </label><input type="text" value = "' + arrayValue[0] + '" name="authors[]" class="authors" placeholder="Nazwisko Imie"> <span class = "firstSpan">,</span><input type="number" class = "shares" value = "' + arrayShares[0] + '"  name="shares[]"><span class = "secondSpan">% udziału </span><div id="simulateSpace"></div>';

    for (var i = 0; i < maxAuthors - 1; i++) {
        authors.innerHTML += '<label class="labelAuthors">Autorzy: </label><input type="text" value = "' + arrayValue[i + 1] + '" name="authors[]" class="authors" placeholder="Nazwisko Imie"> <span class = "firstSpan">,</span><input type="number" class = "shares" value = "' + arrayShares[i + 1] + '" name="shares[]"><span class = "secondSpan">% udziału </span><div class="deleteAuthor" onclick = deleteAuthor(' + (i + 1) + ') >Usuń</div>';
    }

    authors.innerHTML += '<label class="labelAuthors">Autorzy: </label><input type="text" name="authors[]" class="authors" placeholder="Nazwisko Imie"> <span class = "firstSpan">,</span><input type="number" class = "shares" name="shares[]"><span class = "secondSpan">% udziału </span><div class="deleteAuthor" onclick = deleteAuthor(' + maxAuthors + ') >Usuń</div>';

}

function deleteAuthor(index) {
    var authors = document.getElementById("authors");
    var myAuthors = document.getElementsByClassName('authors');
    var maxAuthors = myAuthors.length;
    var myShares = document.getElementsByClassName('shares');
    var myLabel = document.getElementsByClassName('labelAuthors');
    var myAuthor = document.getElementsByClassName('authors');
    var myButton = document.getElementsByClassName('deleteAuthor');
    var mySpan1 = document.getElementsByClassName('firstSpan');
    var mySpan2 = document.getElementsByClassName('secondSpan');

    var arrayValue = [];
    var arrayShares = [];

    for (var i = 0; i < maxAuthors; i++) {
        arrayValue[i] = myAuthors[i].value;
        arrayShares[i] = myShares[i].value;
    }

    myAuthor[index].remove();
    myLabel[index].remove();
    myShares[index].remove();
    mySpan1[index].remove();
    mySpan2[index].remove();
    myButton[index - 1].remove();

    authors.innerHTML = '<label class="labelAuthors">Autorzy: </label><input type="text" value = "' + arrayValue[0] + '" name="authors[]" class="authors" placeholder="Nazwisko Imie"> <span class = "firstSpan">,</span><input type="number" class = "shares" value = "' + arrayShares[0] + '"  name="shares[]"><span class = "secondSpan">% udziału </span><div id="simulateSpace"></div>';

    var j = 0;

    for (var i = 0; i < maxAuthors - 2; i++) {
        if (i == index - 1) j = 1;
        authors.innerHTML += '<label class="labelAuthors">Autorzy: </label><input type="text" value = "' + arrayValue[i + 1 + j] + '" name="authors[]" class="authors" placeholder="Nazwisko Imie"> <span class = "firstSpan">,</span><input type="number" class = "shares" value = "' + arrayShares[i + 1 + j] + '" name="shares[]"><span class = "secondSpan">% udziału </span><div class="deleteAuthor" onclick = deleteAuthor(' + (i + 1) + ') >Usuń</div>';
    }

}